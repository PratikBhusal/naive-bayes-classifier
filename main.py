"""
File: main.py - Naive Bayes Classifier Generator, Printer, and Tester
Authors: Pratik Bhusal and Hrishikesh Inamdar

Description:
This program implements a Naive Bayes Classifier. The program requires 2
command-line arguments in the following order:
    python3 main.py <TRAINING DATA FILE> <TESTING DATA FILE>

Functions Used:
    generate_probs(data)
        This function generates the Naive Bayes Classifier.

        Returns probs: a list of two lists where:
            probs[0][X] = P(C=X)
            probs[1][X][Y][Z] = P(attributeList[X]=Y|C=Z)

        See: test()

    test(data, probs)
        Tests the accuracy of the naiive bayes classifier. data is the test
        data and probs is the naive bayes classifier.

        Returns: Percent of correct / Total Number of Instances.

        See: choose_class()

    choose_class(line,probs):
        Determines the predicated class based on the instance's attribute
        values.

        Returns: The predicted class value.

        See: test()
"""

import sys

attList = []    # this is a list of strings that are the attribute names


# generate_probs(data)
# INPUT: data = a list of lists of lines extracted from the training data
# OUTPUT: probs = a list of two lists:
#       probs[0] = the list of class probabilities
#           probs[0][X] = P(C=X)
#       probs[1] = the list of attribute probabilities
#       probs[1][X][Y][Z] = P(attList[X]=Y|C=Z)
def generate_probs(data):
    # Creates 3-D matrix of height attList.length, width 2, depth 2 so
    # we can store the counts of every attribute value given a class value.
    # Later, we simply divide every value by the number of lines in data to get
    # probabilities.
    # counts[attIndex][valueOfAtt][valueOfClass]
    counts = [[[0, 0], [0, 0]] for i in range((len(attList)-1))]
    countsOfClasses = [0, 0]  # keeps track of counts of class values
    for line in data:
        countsOfClasses[line[-1]] += 1
        for i in range(len(attList)-1):
            counts[i][line[i]][line[-1]] += 1

    # Holds all the probabilities
    probs = [countsOfClasses.copy(), counts.copy()]

    # Calculate Probabilities
    for i in range(len(probs[1])):
        # Divide every attribute count by its respective class count.
        if countsOfClasses[0] != 0:
            probs[1][i][0][0] /= countsOfClasses[0]
            probs[1][i][1][0] /= countsOfClasses[0]
        else:
            probs[1][i][0][0] = 0
            probs[1][i][1][0] = 0
        if countsOfClasses[1] != 0:
            probs[1][i][0][1] /= countsOfClasses[1]
            probs[1][i][1][1] /= countsOfClasses[1]
        else:
            probs[1][i][0][1] = 0
            probs[1][i][1][1] = 0

    # divide every class count by the length of the data
    probs[0][0] /= len(data)
    probs[0][1] /= len(data)
    # Return every single probability, including the class probs where
    # probs[0 = class prob, 1 = atr prob]
    return probs


# choose_class(line,probs)
# INPUT: line = a line of data from the files as a list of 0s and 1s
#       probs = the probs structure from the generate_probs function
# OUTPUT: the predicted class based on the input (0 or 1)
def choose_class(line, probs):
    probC0 = probs[0][0]        # initialize with prob of class being 0
    probC1 = probs[0][1]        # initialize with prob of class being 1
    for i in range(len(line)-1):
        probC0 *= probs[1][i][line[i]][0]
        probC1 *= probs[1][i][line[i]][1]
    if probC0 >= probC1:
        return 0
    else:
        return 1


# test(data,probs)
# a function that simply tests the data given to it and outputs an accuracy
def test(data, probs):
    numCorrect = 0
    for line in data:
        if choose_class(line, probs) == line[-1]:
            numCorrect += 1
    return numCorrect / len(data)


# the main function
if __name__ == "__main__":
    file = open(sys.argv[1])
    # Reads all valid lines (i.e. no lines where it is just whitespace)
    for line in file:
        if not line.isspace():
            attList = line.split()
            break
    trainData = []
    numTrainInstances = 0
    # Insert lines into trainData array
    for line in file:
        if not line.isspace():
            trainData += [list(map(int, line.split()))]
            numTrainInstances += 1

    probs = generate_probs(trainData)

    print("P(class=0)=", format(probs[0][0], '.2f'), sep='', end='')
    for i in range(len(attList)-1):
        print(" P(", attList[i], "=0|0)=", format(probs[1][i][0][0], '.2f'),
              " P(" + attList[i], "=1|0)=", format(probs[1][i][1][0], '.2f'),
              sep='', end='')
    print()
    print("P(class=1)=", format(probs[0][1], '.2f'), sep='', end='')
    for i in range(len(attList)-1):
        print(" P(", attList[i], "=0|1)=", format(probs[1][i][0][1], '.2f'),
              " P(", attList[i], "=1|1)=", format(probs[1][i][1][1], '.2f'),
              sep='', end='')

    print("\n")
    print("Accuracy on training set (", numTrainInstances, " instances): ",
          format(100*test(trainData, probs), '.2f'), "%", sep='')
    print()

    testfile = open(sys.argv[2])
    # Reads all valid lines (i.e. no lines where it is just whitespace)
    for line in testfile:
        if not line.isspace():
            attList = line.split()
            break
    testData = []
    numTestInstances = 0
    # Insert lines into testData array
    for line in testfile:
        if not line.isspace():
            testData += [list(map(int, line.split()))]
            numTestInstances += 1

    print("Accuracy on test set (", numTestInstances, " instances): ",
          format(100*test(testData, probs), '.2f'), "%", sep='')
